Definitions
***********

Application configuration file
    The .cfg file is a RTSC file used to configure the application. It
    typically includes OpenMP configuration as well as configuration for
    other RTSC modules.

Platform file
    The Platform.xdc file that describes memory regions available on
    the target platform. The regions in the platform file are included
    in the linker command file that is created as the result of a RTSC
    build of the platform file.

BIOS
    Short for SYS/BIOS, a scalable real-time kernel

CCS
    Code Composer Studio

IPC
    Inter Process Communication

MCSDK
    Multi-Core Software Development Kit

PSDK
    Processor Software Development Kit

PDK
    Platform Development Kit

QMSS
    Queue Manager Sub System

RTSC
    Real Time Software Components


