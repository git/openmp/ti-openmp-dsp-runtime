Heap Management API
*******************

Heaps in Shared Memory (DDR or MSMC)
====================================

The DSP runtime provides the following APIs to initialize and manage
user defined heaps in shared memory.  These heaps are in addition to
the system heap (the heap used by the malloc, free  and related functions).

.. _heap_init:

Heap Initialization API
-----------------------

The heap initialization functions specified in listing below
must be called by one of the DSP cores to initialize internal heap data
structures before making any memory management calls. Once initialized,
the heaps are accessible by all the DSP cores. These APIs are thread
safe under the OpenMP and OpenCL programming models on the DSP (Each DSP
is running a single thread of execution).

.. note:: 
    If data allocated on the heap is shared across DSP cores, the programmer 
    is responsible for cache consistency of allocated memory across cores. 
    If OpenMP is used to parallelize the program, cache consistency is managed 
    by the OpenMP runtime.

::

    void  __heap_init_ddr(void *ptr, int size);
    void  __heap_init_msmc(void *ptr, int size);


Dynamic Memory Management APIs
------------------------------

After the DDR and/or MSMC heap is initialized by one of the DSP cores
using the API specified in Section :ref:`heap_init`, the following APIs
are available from all DSP core for dynamic memory management:

::

    void *__malloc_ddr   (size_t size);
    void *__calloc_ddr   (size_t num, size_t size);
    void *__realloc_ddr  (void *ptr,  size_t size);
    void  __free_ddr     (void *ptr);
    void *__memalign_ddr (size_t alignment, size_t size);

::

    void *__malloc_msmc   (size_t size);
    void *__calloc_msmc   (size_t num, size_t size);
    void *__realloc_msmc  (void *ptr, size_t size);
    void  __free_msmc     (void *ptr);
    void *__memalign_msmc (size_t alignment, size_t size);


Heap in Local Memory (L2SRAM)
=============================

The DSP runtime provides a simplistic API to initialize a heap in L2 and
allocate from it. This heap is local to the core which initialized it.

Heap Initialization API
-----------------------

A heap can be initialized in L2 using the API in listing
[lst:HeapAllocL2SRAM]. The storage associated with the heap must be
start on a 64bit boundary.

::

    void  __heap_init_l2(void *ptr, int size);

Dynamic Memory Management APIs
------------------------------

After the L2 heap is initialized by the DSP cores using the
__heap_init_l2 call, the only API available is a malloc:

::

    void *__malloc_l2 (size_t size); /* Pointer returned is aligned to 64 bit boundary */


