Integrating Applications Using QMSS
***********************************

OpenMP runtime uses Queue Manager Subsystem (QMSS) for Keystone and Keystone II
processors and initializes it by default. If applications use QMSS outside of OpenMP 
(e.g. to run the networking stack on one of the DSP cores using the NDK), the
initialization must be performed by the application because the runtime
is not aware of QMSS memory regions used by the application.

-  Set openmp.Settings.runtimeInitializesQmss to false
-  Configure QMSS related parameters in the openmp module:

   -  qmssMemRegionIndex
   -  qmssFirstDescIdxInLinkingRam

- Configure the cache settings for the memory regions. Place the following lines after the corresponding base and size variables have been defined in the configuration file:

.. code:: javascript

      var Cache   = xdc.useModule('ti.sysbios.family.c66.Cache');
      Cache.setMarMeta(msmcNcVirt.base, msmcNcVirt.len, 0);
      Cache.setMarMeta(OpenMP.ddrBase, OpenMP.ddrSize, Cache.PC|Cache.PFX|Cache.WTE);
      Cache.setMarMeta(OpenMP.msmcBase, OpenMP.msmcSize, Cache.PC|Cache.PFX|Cache.WTE);

.. note:: 
   1.  OpenMP sets MAR registers in __TI_omp_reset_rtsc_mode
   2.  BIOS resets them to internal defaults during its startup
   3.  OpenMP sets MAR registers again in __TI_omp_initialize_rtsc_mode

   OpenMP performs its QMSS initialization after #3. The application function added via Startup.lastFxns is invoked between #2 and #3. Using the Cache.setMarMeta over-rides the BIOS defaults and ensures that the non-cached region of MSMC is set up correctly.  The QMSS qmssGObj structure is placed in non-cached MSMC. If that region is left as cached, QMSS initialization on the other cores 1, 2 and 3 do not occur correctly (i.e. copying qmssGObj to qmssLObj).

- The application function that performs QMSS initialization (calls ``Qmss_init``) must be added to Startup.lastFxns before ``__TI_omp_initialize_rtsc_mode``.


.. warning::
   On KeyStone devices (TMS3206657 and TMS3206678), memory regions must be added in increasing order of memory addresses. The openmp module configuration parameters qmssMemRegionIndex and qmssFirstDescIdxInLinkingRam can be used to enforce this restriction. Refer QMSS LLD API for details.

.. note::
   The integrating_qmss example in ``openmp_dsp_2_02_xx_xx/packages/examples`` illustrates configuring the OpenMP runtime such that QMSS initialization is performed outside the OpenMP runtime.


