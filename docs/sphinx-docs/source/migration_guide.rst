Migration Guide
***************

This page describes how to port an application written using OpenMP Runtime 1.x to OpenMP Runtime 2.x.

Key Differences between OpenMP Runtime 1.x and 2.x
==================================================

OpenMP runtime 2.x is configured by setting parameters in
the OpenMP RTSC module. Refer to the sample application configuration
file for details.

Porting an OpenMP Runtime 1.x Application to 2.x
================================================

#. Switch to a platform file (Platform.xdc) supplied with 2.x

#. Use the provided example application configuration file as a starting
   point to port OpenMP runtime 1.x configuration to corresponding 2.x
   parameters.

#. If you are modifying the memory ranges specified in Platform.xdc:
   - Update OpenMP module parameters in the application configuration file to correspond to the new Platform file.

#. OpenMP Runtime 1.x invokes the application main function as a BIOS
   Task and calls ``BIOS_start()`` before main. OpenMP runtime 2.x
   does not call main in the context of a BIOS Task. If the application
   uses Tasks, it must call ``BIOS_start()``.


