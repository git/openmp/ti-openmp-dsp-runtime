Resource Usage
**************

The table below lists resource usage of the runtime on Keystone and Keystone II processors with the
default configuration.

===================   ======================   ===========================================
Resource              Description              OMP runtime usage          
===================   ======================   ===========================================
L1P                   Level 1 Program cache    Configured as cache (32KB)
L1D                   Level 1 Data cache       Configured as cache (32KB)
L2 (cache)            Level 2 cache            128K configured as cache in __TI_omp_reset
L2 (memory)           Level 2 scratch          171KB is used as scratch. 32KB is used for
                                               the core local heap( configured via 
                                               localHeapSize) and 128KB is stack.
MSMCSRAM cached       On-chip shared memory    Not used by the runtime.
                                               Application can use for code/data. 
                                               Must be marked write-through (Refer
                                               __TI_omp_reset)
MSCMSRAM non-cached   On-chip shared memory    128KB must be non-cached
DDR                   Off-chip shared memory   Application heap is mapped to DDR in
                                               omp_config.cfg. The size of the heap 
                                               is controlled by program.heap and the section it is 
                                               mapped to depends on the mapping of section .sysmem. 
                                               Any DDR regions annotated cached by the application 
                                               must also be annotated write-through (see __TI_omp_reset)
Hardware Semaphores   Semaphore module         7 semaphores, starting at hwSemBaseIdx 
Hardware Queues       QMSS General purpose     11 hardware queues starting at qmssHwQueueBaseIdx
                      queues
Memory Regions        QMSS Memory Regions      1 memory region, specified by qmssMemRegionIdx
===================   ======================   ===========================================


The table below lists resource usage of the runtime on Sitara processors with the
default configuration.

===================   ======================   ===========================================
Resource              Description              OMP runtime usage          
===================   ======================   ===========================================
L1P                   Level 1 Program cache    Configured as cache (32KB)
L1D                   Level 1 Data cache       Configured as cache (32KB)
L2 (cache)            Level 2 cache            128K configured as cache in __TI_omp_reset
L2 (memory)           Level 2 scratch          160KB 
                                               is used as scratch. 32KB is used for
                                               the core local heap( configured via 
                                               allocLocalHeapSize) and 32KB is stack.
DDR non-cached        Off-chip shared memory   128KB must be non-cached
DDR                   Off-chip shared memory   Application heap is mapped to DDR in
                                               omp_config.cfg. The size of the heap 
                                               is controlled by program.heap and the section it is 
                                               mapped to depends on the mapping of section .sysmem. 
                                               Any DDR regions annotated cached by the application 
                                               must also be annotated write-through (see __TI_omp_reset)
Hardware Spin Locks   Spin Lock module         7 spin locks, starting at hwSemBaseIdx 
===================   ======================   ===========================================

