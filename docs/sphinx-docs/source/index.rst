############################
TI OpenMP DSP v2.2.x
############################

TI |OpenMP (R)| DSP Documentation Contents:

.. toctree::
   :maxdepth: 2

   intro
   building_openmp_app
   heap_management_api
   configuring_runtime
   openmp_accelerator
   opencl_mode
   implementation_defined
   resource_usage
   reducing_footprint
   integrating_apps_with_qmss
   migration_guide
   defect_fixes
   known_issues
   definitions
   disclaimer

.. image:: images/platform_red.png

.. |OpenMP (R)| unicode:: OpenMP U+00AE
