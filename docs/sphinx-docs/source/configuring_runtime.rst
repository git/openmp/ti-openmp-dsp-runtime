Configuring the Runtime
***********************

The OpenMP module provides various parameters that are used to configure
the runtime. These parameters are described in the sections below.

Configuring Cores
-----------------

The listing below configures the index of the master core and the
number of cores available to the runtime. The set of cores must be
contiguous and can be smaller than the number of cores available on the
device. E.g. Cores 0-3, Cores 1-6 etc.

::

    var OpenMP = xdc.useModule('ti.runtime.ompbios.OpenMP');
    OpenMP.masterCoreIdx = 0;
    OpenMP.numCores      = 8;

Configuring Memory Regions
--------------------------

The OpenMP runtime needs to know the memory ranges corresponding to the various
memory regions described in the platform file. It uses this information to set
the appropriate caching attributes for the regions.

Keystone and Keystone II processors have onchip MSMC memory.  Part of 
this memory is configured as non-cacheable and is used by the OpenMP
runtime to store shared state.  

::

    // Pull in memory ranges described in Platform.xdc to configure the runtime
    var ddr3       = Program.cpu.memoryMap["DDR3"];
    var msmc       = Program.cpu.memoryMap["MSMCSRAM"];
    var msmcNcVirt = Program.cpu.memoryMap["OMP_MSMC_NC_VIRT"];
    var msmcNcPhy  = Program.cpu.memoryMap["OMP_MSMC_NC_PHY"];

    // Initialize the runtime with memory range information
    OpenMP.msmcBase = msmc.base;
    OpenMP.msmcSize = msmc.len;

    OpenMP.msmcNoCacheVirtualBase  = msmcNcVirt.base;
    OpenMP.msmcNoCacheVirtualSize  = msmcNcVirt.len;

    OpenMP.msmcNoCachePhysicalBase  = msmcNcPhy.base;

    OpenMP.ddrBase          = ddr3.base;
    OpenMP.ddrSize          = ddr3.len;

Sitara devices do not have MSMC memory. The OpenMP runtime uses   
a region of non-cacheable DDR memory to store shared state.

::

    // Pull in memory ranges described in Platform.xdc to configure the runtime
    var ddr3       = Program.cpu.memoryMap["DDR3"];
    var ddr3_nc    = Program.cpu.memoryMap["DDR3_NC"];

    OpenMP.msmcBase = msmc.base;
    OpenMP.msmcSize = msmc.len;

    OpenMP.ddrNoCacheBase  = ddr3_nc.base;
    OpenMP.ddrNoCacheSize  = ddr3_nc.len;

    OpenMP.ddrBase          = ddr3.base;
    OpenMP.ddrSize          = ddr3.len;


Configuring the Heap
--------------------

The OpenMP runtime can use the HeapOMP module to configure and initialize heap
memory. This module handles memory allocation requests:

-  From BIOS components
-  From malloc and memalign

HeapOMP maintains a core local heap (HeapMem) and a shared heap
(HeapMemMP). The core local heap is used to satisfy memory allocation
requests before the shared heap is intialized and available. The shared
heap is implemented via a heap in a Shared Region that is created during
IPC startup.

::

    var HeapOMP = xdc.useModule('ti.runtime.ompbios.HeapOMP');

    // Shared Region 0 must be initialized for IPC 
    var sharedRegionId = 0;

    // Size of the core local heap
    var localHeapSize  = 0x8000;

    // Size of the heap shared by all the cores
    var sharedHeapSize = 0x8000000;

    // Initialize a Shared Region & create a heap in the DDR3 memory region
    var SharedRegion   = xdc.useModule('ti.sdo.ipc.SharedRegion');
    SharedRegion.setEntryMeta( sharedRegionId,
                               {   base: ddr3.base,
                                   len:  sharedHeapSize,
                                   ownerProcId: OpenMP.masterCoreIdx,
                                   cacheEnable: true,
                                   createHeap: true,
                                   isValid: true,
                                   name: "DDR3_SR0",
                               });

    // Configure and setup HeapOMP
    HeapOMP.configure(sharedRegionId, localHeapSize);

The OpenMP runtime can also be configured to use a shared heap that is
allocated out of the .sysmem section.  This type of shared heap does not
require IPC.  This type of heap can be used in by setting the
OpenMP.useIpcSharedHeap flag to false.

::

       OpenMP.useIpcSharedHeap = false;
       OpenMP.allocateLocalHeapSize = 0x8000
       OpenMP.allocateSharedHeapSize = 0x00800000

Configuring Reset and Startup functions
---------------------------------------

.. cpp:function:: void __TI_omp_reset_rtsc_mode()

  The reset function sets up memory attributes for the various regions
  specified in the platform file using corresponding parameters from the OpenMP
  module. The reset function is invoked via the XDC Reset hook:

::

    var Reset = xdc.useModule('xdc.runtime.Reset');
    Reset.fxns.$add('&__TI_omp_reset_rtsc_mode');

.. cpp:function:: void __TI_omp_initialize_rtsc_mode()

  This function configures the runtime and initializes QMSS hardware
  queues required by the runtime. It also starts the runtime on worker
  cores. This function is invoked via the XDC Startup hook mechanism
  (xdc.runtime.Startup).

::

    // __TI_omp_initialize_rtsc_mode configures the runtime and calls main
    var Startup = xdc.useModule('xdc.runtime.Startup');
    Startup.lastFxns.$add('&__TI_omp_initialize_rtsc_mode');

This function is added in the application configuration file to give the
programmer flexibility to order Startup.lastFxns appropriately. For
example, if the application is initializing QMSS, the call to initialize
QMSS must be added to lastFxns before __TI_omp_initialize_rtsc_mode is added.


.. warning::
    Unlike OpenMP Runtime 1.x, this runtime does not invoke main() in the context 
    of a BIOS Task nor does it start the BIOS scheduler via BIOS_start().



Platform file
-------------

A RTSC platform provides information about device memory, peripherals,
clock speed and external off-chip memory.

Device Name
~~~~~~~~~~~

The supplied RTSC application configuration files rely on the device
name to configure the number of cores used by the OpenMP runtime
correctly. Device names are obtained from the platform file and should
match the desired target.

CPU Clock Frequency
~~~~~~~~~~~~~~~~~~~

The OpenMP runtime timing functions, ``omp_get_wtime()`` and
``omp_get_wtick()``, require information about the operating frequency of
the CPUs (in MHz). In RTSC mode, the CPU clock frequency is obtained
from the platform file. Therefore, users must ensure that the CPU clock
frequency specified in the platform file is correct.

Memory Regions
~~~~~~~~~~~~~~

Default sizes for level 1 program cache, level 1 data cache and level 2
cache are obtained from the platform file.

Platform files used with the OpenMP runtime must define the following
memory regions:

L2SRAM 
  Level 2 SRAM local to each core configured as scratch. At
  least 64K of L2 must be configured as scratch to hold core-specific
  runtime variables. If the application stack is placed in L2SRAM, it must
  be sized taking application stack requirements into account.

OMP_MSMC_NC_VIRT
  This region is mapped to shared on-chip memory (MSMC SRAM) and caching
  is disabled using the MAR registers corresponding to the memory region. 
  The OpenMP runtime requires 128KB of MSMC SRAM to be non-cached to 
  store internal data structures.

  Since the MAR register corresponding to MSMC address range does not
  permit caching to be disabled, an MPAX register is used to map a portion
  of MSMCSRAM (in this case, 128KB) into an unused address space
  (0xa0000000 on the C6678 EVM) and the MAR register corresponding to
  0xa0000000 is used to disable caching.

MSMCSRAM
  Shared on-chip memory. Can be used for code, data or heap

DDR3
 Shared off-chip memory. Used to hold Thread Local Storage (TLS) initialization 
 sections. Can also be used for code, shared data or the heap.

A sample C6678 Platform file is shown in the listing below. 

.. literalinclude:: ../../../src/ti/runtime/openmp/platforms/evm6678/Platform.xdc
    :lines: 42-77
    :linenos:

The OpenMP runtime supplies platform files for the various devices it 
supports in the ti.runtime.openmp.platforms directory.
