Building an OpenMP Application
******************************

This page describes the steps for building OpenMP applications for
supported devices. Section :ref:`build_prereq` discusses additional
software components that are required for building OpenMP applications.
Sections :ref:`using_ccs` and :ref:`using_makefiles` describe building
OpenMP applications within CCS and using makefiles.

.. note::
   Running OpenMP applications on simulators is not supported.

.. _build_prereq:

Build Prerequisites
-------------------

TMS320C6000 Optimizing Compiler v8.0.0 or greater is required to build
OpenMP applications. In addition, the following components are required:

* PDK
* BIOS
* IPC 
* XDC

The components, except for the compiler, are bundled in the Processor Software
Development Kit (referred to as the SDK).


.. _using_ccs:

Building Applications within CCS
--------------------------------

CCS and SDK installations are required in order to build applications
within CCS. For directions on downloading and installing CCS, see
`CCS download <http://processors.wiki.ti.com/index.php/Download_CCS>`_.

Various versions of the Processor SDK are available by following the
appropriate installation instructions link in the device support table (See
Section :ref:`devices_supported`).

Note that the required software components (PDK, BIOS, IPC and XDC) and the
OpenMP runtime (OMP), must be discovered as RTSC products within CCS.  Also,
the correct compiler version must be discovered in CCS. 

The following instructions are based on CCSv5.4.

-  Create a new RTSC project (Empty RTSC project under Project templates
   and examples. 

   -  Family: C6000

   -  Variant: TMS320C66XX, TCI6636K2H (or) C66xx Multicore DSP,
      TMS320C6678, TMS320C6657, AM572

   -  Set Output Format under Advanced Settings to eabi (ELF)

.. figure:: images/new_rtsc_project.png
   :align: center
   :scale: 70%

   Creating a new RTSC project

-  Add the SDK packages to the RTSC products and repositories available to CCS. 
   (under :menuselection:`Window --> Preferences --> Code Composer Studio --> RTSC`)

-  Add the OpenMP runtime, BIOS and IPC to the project's list of RTSC modules.
   (under :menuselection:`Project --> Properties --> General --> RTSC`)

-  If building for C6678/C6657, add the C6678/C6657 PDK. If
   building for TCI6636, add the KeyStone2 PDK.  If building for AM572,
   add the AM57x PDK.

-  Target: ti.targets.elf.C66

-  Platform:

   -  C6678: ``ti.runtime.openmp.platforms.evm6678``

   -  C6657: ``ti.runtime.openmp.platforms.evm6657``

   -  TMS320TCI6636: ``ti.runtime.openmp.platforms.evmTCI6636K2H``

   -  AM572: ``ti.runtime.openmp.platforms.am57x``

-  Build Profile: release or debug

-  Enable the ``--openmp`` compiler option 
   (under :menuselection:`Advanced options --> Advanced Optimizations`)

-  Enable the –priority linker option 
   (under :menuselection:`C6000 Linker --> File Search Path --> Search libraries in priority order`)

-  Add the source files (e.g. packages/examples/hello_with_make/hello.c)

-  Add a configuration file (packages/examples/hello_with_make/omp_config.cfg)

-  Build the application

.. _using_makefiles:

Building Applications Using Makefiles
-------------------------------------

An OpenMP applications can be built outside of CCS using
Makefiles. An example is located at ``packages/examples/hello_with_make``. 
This example uses 2 Makefiles:

**Makefile.libomp**

- Used for  RTSC build. This build takes a configuration file 
  (e.g. ``packages/examples/hello_with_make/omp_config.cfg``) and platform file 
  (e.g. ``ti.runtime.openmp.platforms.evm6678``) as input and generates a compiler options file 
  (omp_config/compiler.opt) and linker command file (``omp_config/linker.cmd``).
- Set BUILD_TYPE to release or debug to include the appropriate OpenMP runtime libraries
- Set the following environment variables to the locations of corresponding components:

  - OMP_DIR
  - C6678_PDK_DIR/C6657_PDK_DIR/C6636_PDK_DIR/AM572_PDK_DIR based on the device
  - XDC_DIR
  - XDCCGROOT (path to C6000 compiler tools)
  - BIOS_DIR and IPC_DIR (if in BIOS mode)

**Makefile**

- Used to build the OpenMP application. Includes Makefile.libomp and adds the appropriate dependencies to run the RTSC build before building the application.
- Set OMP_TARGET to one of C6678, C6657, C6636 or AM572
- Set USE_BIOS to 1 if application uses BIOS or other RTSC modules, 0 otherwise


**omp_config.cfg**
  A sample configuration file. 

**omp_config_api.c**
  A sample implementation of the configuration APIs.


Running applications within CCS
*******************************

When running OpenMP applications within CCS, the user has to 

#. Connect to the number of cores required by the runtime (e.g. 8 on C6678)


   -  Creating a group with all DSP cores makes it easier to connect
      to all cores

   -  C6678/C6657: After connecting, run the
      Global_Default_Setup script on Core 0 to
      initialize the device (Select Core 0 in the Group, then 
      :menuselection:`Scripts --> EVM6678L Init Function --> Global_Default_Setup`)
#. Load the application on all cores
#. Run the application across all cores

.. warning::
   It is critical to load the application on **all** cores before running the application on **any** core. Failure to do so can leave initialized data shared across the cores in an undefined state leading to execution errors. 
   
   In order to ensure that all cores are loaded before any core starts running, disable "Run to symbol" in debug configuration for **all** DSP cores.
  
.. figure:: images/omp_disabling_run_to_main.png
   :align: center
   :scale: 70%
