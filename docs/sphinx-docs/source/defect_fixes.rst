############
Defect Fixes
############

1. Configuring the OpenMP runtime in RTSC mode to use a subset of the cores starting with a non zero master core index (OpenMP.masterCoreIdx) does not work as expected. This is fixed in OpenMP Runtime version 2.01.17.02.

