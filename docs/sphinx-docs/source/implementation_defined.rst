OpenMP 3.0 Implementation-Defined Behaviors
*******************************************

The OpenMP 3.0 specification identifies some features as implementation
defined. The following documents the behaviour of the runtime with
respect to implementation-defined features.

-  Nested Parallelism: Only one level of parallelism is supported.
   Nested parallel regions will be executed by teams comprising only one
   thread.

-  Task Scheduling Points in Untied Tasks: Only tied tasks are currently
   supported and untied tasks are treated as tied tasks. Therefore, task
   scheduling points in untied task regions occur at the same points as
   in tied task regions.

-  Memory Model: Memory accesses by multiple threads to the same
   variable without synchronization may not be atomic with respect to
   each other.

-  Dynamic adjustment of threads: Dynamic adjustment of the number of
   threads is not supported. When a thread encounters a parallel
   construct, the number of threads delivered by this implementation is
   determined according to Algorithm 2.1 in the OpenMP 3.0
   Specification. When more threads are requested than are available,
   the available threads are supplied.

-  Loop directive: The effect of ``schedule(runtime)`` clause when the
   *run-sched-var* ICV is set to auto is static with no chunk size.

-  Constructs

   -  ``sections`` construct: The structured blocks in a ``sections``
      construct are assigned to the members of the team executing the
      sections region, such that the threads execute an approximately
      equal number of sections.

   -  ``single`` construct: The first thread that encounters a
      ``single`` construct executes its structured block.

-  Runtime library routines

   -  ``omp_set_num_theads``: If the argument is not a positive integer,
      then the call is ignored.

   -  ``omp_set_schedule``: Only valid OpenMP schedule kinds are accepted.

   -  ``omp_set_max_active_levels``: When called from within an explicit
      parallel region, the binding thread set is all threads. However,
      since nested parallelism is not supported, this implementation has
      little use for this function. If the argument is a negative
      integer or a positive integer greater than one, then the call is
      ignored.

   -  ``omp_get_max_active_levels``: This routine may be called anywhere
      in the program. It always returns the value of the
      gomp_max_active_levels_var internal control variable.


