OpenMP DSP Runtime
------------------
1. Set up the paths to various dependencies in utils/product/Makefile.inc 
2. Building the product in the exports directory. version number is specified
   in utils/product/version.txt
   make -f utils/product/Makefile .product
3. Build a zip file with the product
   make -f utils/product/Makefile .zipfile
