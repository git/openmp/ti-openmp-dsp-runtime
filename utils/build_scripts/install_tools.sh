#!/bin/bash

if [ $# -ne 1 ]
then
echo "Usage: $0 <install_dir>"
exit -1
fi

mkdir -p $1

cd $1

MCSDK3_VER=3_00_03_15
if [ -d "mcsdk_bios_$MCSDK3_VER" ]
then
echo MCSDK $MCSDK3_VER already installed
else
echo Installing MCSDK $MCSDK3_VER ...
wget --no-proxy "http://downloads.ti.com/sdoemb/sdoemb_public_sw/mcsdk/"$MCSDK3_VER"/exports/mcsdk_"$MCSDK3_VER"_setuplinux.bin"
chmod +x "mcsdk_"$MCSDK3_VER"_setuplinux.bin"
"./mcsdk_"$MCSDK3_VER"_setuplinux.bin" --prefix $1 --mode silent
fi

#MCSDK2_VER=02_01_03_07
#MCSDK2_VER1=2_01_03_07
#if [ -d "mcsdk_$MCSDK2_VER1" ]
#then
#echo MCSDK $MCSDK2_VER1 already installed
#else
#echo Installing MCSDK $MCSDK2_VER1
#wget --no-proxy "http://10.218.101.172/nightly_builds/bios-mcsdk-2.1.1/607-2014-01-27_14-06-20/artifacts/output/bios_mcsdk_"$MCSDK2_VER"_setuplinux.bin"
#chmod +x "bios_mcsdk_"$MCSDK2_VER"_setuplinux.bin"
#"./bios_mcsdk_"$MCSDK2_VER"_setuplinux.bin" --prefix $1 --mode silent
#fi

CGT_VER=7.4.6
if [ -d "TI_CGT_C6000_$CGT_VER" ]
then
echo CGTOOLS $CGT_VER already installed
else
echo Installing CGTOOLS $CGT_VER
CGT_VER_U=`echo $CGT_VER | sed 's|\.|_|g'`
wget -nc "http://syntaxerror.dal.design.ti.com/release/releases/c60/rel"$CGT_VER_U"/build/install/ti_cgt_c6000_"$CGT_VER"_setup_linux_x86.bin"
chmod +x "ti_cgt_c6000_"$CGT_VER"_setup_linux_x86.bin"
"./ti_cgt_c6000_"$CGT_VER"_setup_linux_x86.bin" --prefix  $1/TI_CGT_C6000_$CGT_VER --mode silent
fi



