#!/bin/bash

usage() {
 echo "Usage: ./build_auto.sh <git_tag> <version_string>"
 echo
 echo "Example: ./build_auto.sh HEAD 2_01_12_00"
}

if [ -z $2 ]
then
usage
exit
fi

SCRIPTS_HOME=`pwd`

echo "Script dir: $SCRIPTS_HOME"

OPENMP_RUNTIME_TAG=$1
#OPENMP_RUNTIME_TAG=HEAD
VERSION_STRING=$2
#export VERSION_STRING=2_01_12_00

ROOT_DIR=$SCRIPTS_HOME/builds
echo "ROOTDIR = $ROOT_DIR"

RELEASE_VERSION=openmp_dsp_$VERSION_STRING

VERSION_STRING_DOT=`echo $VERSION_STRING | sed -e 's|_|.|g'`
#DEBIAN_TARBALL=openmp-dsp_$VERSION_STRING_DOT.orig.tar.gz
DEBIAN_TARBALL=$RELEASE_VERSION.tar.gz

mkdir -p $ROOT_DIR || { echo "Error: Could not mkdir $ROOT_DIR"; exit -1; }

rm -rf $SCRIPTS_HOME/artifacts*

mkdir -p $SCRIPTS_HOME/artifacts/logs
mkdir -p $SCRIPTS_HOME/artifacts/output

## Write expected output files ##
echo "${RELEASE_VERSION}.zip" >  $SCRIPTS_HOME/artifacts/output/build_targets
echo "${DEBIAN_TARBALL}" >>  $SCRIPTS_HOME/artifacts/output/build_targets

## Clone the MCSDK-HPC GIT ##
echo "`date` : Cloning openmp-runtime GIT..." >> $SCRIPTS_HOME/artifacts/logs/status.log
cd $ROOT_DIR
git clone git://gitorious.design.ti.com/openmp/openmp-runtime.git ./openmp_runtime_$OPENMP_RUNTIME_TAG

cd ./openmp_runtime_$OPENMP_RUNTIME_TAG

## Reset HEAD to the tag ##
echo "`date` : Resetting openmp-runtime GIT to $OPENMP_RUNTIME_TAG..." >> $SCRIPTS_HOME/artifacts/logs/status.log
git reset --hard $OPENMP_RUNTIME_TAG

## Capture Commit ID ##
echo -n openmp-runtime: >> $SCRIPTS_HOME/artifacts/repo-revs.txt
echo -n `git rev-parse HEAD`: >> $SCRIPTS_HOME/artifacts/repo-revs.txt
echo -n `git branch | grep "*" | sed -e 's|^* ||g'` >> $SCRIPTS_HOME/artifacts/repo-revs.txt
echo :$OPENMP_RUNTIME_TAG >> $SCRIPTS_HOME/artifacts/repo-revs.txt

sudo apt-get -y install doxygen
sudo apt-get -y install texlive-fonts-recommended

## Install openmp-runtime dependencies, such as MCSDK and CGTOOLS
export TI_INSTALL_DIR=$ROOT_DIR/test/ti

cd utils/build_scripts
echo "`date` : Installing dependencies..." >> $SCRIPTS_HOME/artifacts/logs/status.log
/bin/bash ./install_tools.sh $ROOT_DIR/test/ti

echo "`date` : Setup environment variables..." >> $SCRIPTS_HOME/artifacts/logs/status.log
source setup_env.sh

## Generate the release pacakge ##
echo "`date` : Creating zip file with the product..." >> $SCRIPTS_HOME/artifacts/logs/status.log
#Used to locate package release directory
export OMP_DIR=$OMP_BASE_DIR/exports/${RELEASE_VERSION}/packages
/bin/bash ./build_package.sh 2>&1 | tee $SCRIPTS_HOME/artifacts/logs/build.log
cp $OMP_BASE_DIR/exports/${RELEASE_VERSION}.zip $SCRIPTS_HOME/artifacts/output/

## Create tarball for PPA ##
echo "`date` : Preparing tarball for PPA..." >> $SCRIPTS_HOME/artifacts/logs/status.log
cp ../deb-pkg/Makefile $OMP_BASE_DIR/exports/${RELEASE_VERSION}
cp -r ../deb-pkg/debian $OMP_BASE_DIR/exports/${RELEASE_VERSION}
cd $OMP_BASE_DIR/exports
tar -czvf $DEBIAN_TARBALL ${RELEASE_VERSION}
cp $DEBIAN_TARBALL $SCRIPTS_HOME/artifacts/output/

echo "`date` : Complete." >> $SCRIPTS_HOME/artifacts/logs/status.log

cd $SCRIPTS_HOME
tar -czvf artifacts.tgz artifacts

