#!/bin/bash

#Used to locate packages provided by openmp-runtime
export OMP_BASE_DIR=$(pwd)/../..

# Common base directory for all TI Software (Individual package paths may be modified below as needed)
if [ -z $TI_INSTALL_DIR ]
then
  TI_INSTALL_DIR=~/ti
fi

unset C6670_PDK_DIR
unset C6678_PDK_DIR
unset C6657_PDK_DIR

# Set KS1 related environment variables only if we're building for
# KS1 devices.
if [[ ${BUILD_KS1_LIBS} == 1 ]]
then
export C6678_PDK_DIR=$TI_INSTALL_DIR/pdk_C6678_2_1_3_7/packages
export C6670_PDK_DIR=$TI_INSTALL_DIR/pdk_C6670_2_1_3_7/packages
export C6657_PDK_DIR=$TI_INSTALL_DIR/pdk_C6657_2_1_3_7/packages
fi

export ULM_DIR=$LINUX_DEVKIT_ROOT/usr/include


export C6636_PDK_DIR=$TI_INSTALL_DIR/pdk_keystone2_3_00_03_15/packages

export XDCCGROOT=$TI_INSTALL_DIR/TI_CGT_C6000_7.4.6

export BIOS_DIR=$TI_INSTALL_DIR/bios_6_35_04_50/packages
export IPC_DIR=$TI_INSTALL_DIR/ipc_3_00_04_29/packages
export XDC_DIR=$TI_INSTALL_DIR/xdctools_3_25_02_70

